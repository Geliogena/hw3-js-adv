/*
Деструктуризація — це корисна функція, яка полегшує вилучення даних з масивів або об'єктів, дозволяючи присвоювати
 їх значення змінним. Цей синтаксмс дозволяє писати компактніший код, зменшуючи необхідність вручну витягувати дані, яких
 може бути багато.
 */
// 1 завдання
"use strict"
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
const [ ,second, , fourth] = clients2;
const clients3 = [...clients1, ...[second, fourth]];
console.log(clients3);

// 2 завдання
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vampire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

const result = characters.map(function (obj){
     const {name, lastName, age} = obj;
     return {name, lastName, age};
});
const charactersShortInfo = [...result];
console.log(charactersShortInfo);

// 3 завдання

const user1 = {
    name: "John",
    years: 30
};
let name, age, isAdmin;
({name, years: age, isAdmin = false} = user1);
alert(name + " " + age + " " + isAdmin);


// 4 завдання
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

const fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};
console.log(fullProfile);

// 5 завдання
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}


const newArr =  [...books, bookToAdd];
console.log(newArr);

// 6 завдання
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}
const newProperties = {...employee, age: 54, salary: 30000};
console.log(newProperties);

// 7 завдання
const array = ['value',  ()=> 'showValue'];
const [value, showValue] = array;
 alert(value);
 alert(showValue());
